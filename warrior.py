from fighter import Fighter
class Warrior(Fighter):
    def __init__(self):
        self._damagePoints= None
        self._isVulnerable= False
        self._lifePoint = 50
        

    @property
    def damagePoints(self):
        return self._damagePoints 
    @damagePoints.setter
    def damagePoints(self,new_damagePoints):
        self._damagePoints=new_damagePoints
    @property
    def isVulnerable(self):
        return self._isVulnerable
    @isVulnerable.setter
    def isVulnerable(self,new_isVulnerable):
        self._isVulnerable=new_isVulnerable
    @property
    def lifePoint(self):
        return self._lifePoint
    @lifePoint.setter
    def lifePoint(self,new_lifePoint):
        self._lifePoint=new_lifePoint
    def toString(self):
        super().toString(self)
        print ("Tipo_Warrior")
    def vulnerable(self,enemigo):
        if enemigo.isVulnerable is True:
            self.damagePoints=10
            print("El mago es vulnerable, asi que el daño del guerrero se fija en 10")
        else:
            self.damagePoints=6
            print("El mago no es vulnerable, asi que el daño del guerrero se fija en 3")    
    def ataque(self,enemigo):
        enemigo.lifePoint= enemigo.lifePoint - self.damagePoints
    
