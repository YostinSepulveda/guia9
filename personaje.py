from abc import abstractclassmethod
from abc import ABCMeta

class Personaje(metaclass=ABC):
    @abstractclassmethod
    def damagePoints(self,damagePoints):
        return damagePoints
    @abstractclassmethod
    def isVulnerable(self,isVulnerable):
        return isVulnerable
    @abstractclassmethod
    def _lifePoint(self,LP):
        return LP