import random
from warrior import Warrior
from wizzard import Wizzard

class Juego():
        
    def juego(self):
        
        Wizzard1=Wizzard()
        Warrior1=Warrior()
        
        print("Empieza la pelea entre 2 luchadores, un gran mago y un gran guerrero")
        primero=random.randint(0,1)
        Wizzard1.prepararSpell(a=random.randint(0,1))
        Wizzard1.vulnerable()
        if primero==1:
            print("El primer luchador en atacar es el mago")
            enemigo=Warrior1
            Wizzard1.ataque(enemigo)
            print("La nueva vida del guerrero es",Warrior1.lifePoint)
        else:
            print("El primer luchador en atacar es el guerrero")
            enemigo=Wizzard1
            Warrior1.vulnerable(enemigo)
            Warrior1.ataque(enemigo)
            print("La nueva vida del mago es",Wizzard1.lifePoint)
        x=0
        y=None
        
        while x==0:
            print("----------------------------------")
            y==random.randint(0,1)
            Wizzard1.prepararSpell(a=random.randint(0,1))
            Wizzard1.vulnerable()
            if y==1:
                print("El siguiente luchador en atacar es el mago")
                enemigo=Warrior1
                Wizzard1.ataque(enemigo)
                print("La nueva vida del guerrero es",Warrior1.lifePoint)
                if Wizzard1.lifePoint<=0:
                    print("El ganador es el guerrero")
                    x=0
                else:
                    pass    
            else:
                print("El siguiente luchador en atacar es el guerrero")
                enemigo=Wizzard1
                Warrior1.vulnerable(enemigo)
                Warrior1.ataque(enemigo)
                print("La nueva vida del mago es",Wizzard1.lifePoint)
                if Warrior1.lifePoint<=0:
                    print("El ganador es el guerrero")
                    x=0
                else:
                    pass             
            
            
if __name__ == "__main__":
    juego = Juego()
    juego.juego()
    