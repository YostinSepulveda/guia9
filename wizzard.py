import random
from fighter import Fighter
class Wizzard(Fighter):
    def __init__(self):
        self._damagePoints= None
        self._isVulnerable= True
        self._prepareSpell= False
        self._lifePoint = 40

    @property
    def damagePoints(self):
        return self._damagePoints 
    @damagePoints.setter
    def damagePoints(self,new_damagePoints):
        self._damagePoints=new_damagePoints
        
    @property
    def isVulnerable(self):
        return self._isVulnerable
    
    @isVulnerable.setter
    def isVulnerable(self,new_isVulnerable):
        self._isVulnerable=new_isVulnerable
        
    @property
    def lifePoint(self):
        return self._lifePoint
    @lifePoint.setter
    def lifePoint(self,new_lifePoint):
        self._lifePoint=new_lifePoint
    def toString(self):
        
        super().toString(self)
        print ("Tipo_Wizzard")
    def prepararSpell(self,a):
        if a==1:
            self.prepareSpell=True
            print("El mago tiene un hechizo preparado su daño se fija en 12 puntos y no es vulnerable")
        else:
            self.prepareSpell=False
            print("El mago no tiene un hechizo preparado su daño se fija en 3 puntos y es vulnerable")
    def vulnerable(self):
        if self.prepareSpell==True:
            self.damagePoints=12
            self.isVulnerable=False
        else:
            self.damagePoints=3
            self.isVulnerable=True          
    def ataque(self,enemigo):
        enemigo.lifePoint= enemigo.lifePoint - self.damagePoints
    