from abc import ABC
from abc import abstractclassmethod
class Fighter(ABC):
    @abstractclassmethod
    def damagePoints(self,damagePoints):
        return damagePoints
    @abstractclassmethod
    def isVulnerable(self,isVulnerable):
        return isVulnerable
    @abstractclassmethod
    def lifePoint(self,lifePoint):
        return lifePoint
    @abstractclassmethod
    def toString(self,toString):
        print("Tipo_Fighter")